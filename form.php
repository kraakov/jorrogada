<!DOCTYPE html>
<html lang="ca">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Formulario</title>
<style>
.error {
	color: #FF0000;
	background-color: black;
	border-radius: 2px;
	}
</style>
</head>
<body>
<?php
$nomErr = $emailErr = $estudisErr = $comentariErr = "";
$nom = $email = $estudis = $comentari = "";
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (empty($_POST["nom"])) {
    $nomErr = "El nom es requerit";
  } else {
    $nom = test_input($_POST["nom"]);
    if (!preg_match("/^[a-zA-Z ]*$/",$nom)) {
      $nameErr = "Sols lletres i espais en blanc"; 
    }
  }
  if (empty($_POST["email"])) {
    $emailErr = "Email es requerit";
  } else {
    $email = test_input($_POST["email"]);
    // check if e-mail address is well-formed
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $emailErr = "Format d'email incorrecte"; 
    }
  }
  if (empty($_POST["estudis"])) {
    $estudisErr = "El camp estudis es requerit";
  } else {
    $estudis = test_input($_POST["estudis"]);
  }

  if (empty($_POST["comentari"])) {
    $comentariErr = "El comentari es requerit";
  } else {
    $comentari = test_input($_POST["comentari"]);
  }
}
function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}?>
<h1>FORMULARI ACCESIBLE</h1>
	<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
	<fieldset>
	<legend>Formulari de consulta</legend>
	<p><span class="error">* camp requerit.</span></p>
		<label for="nom">Nom:</label>
		<input type="text" id="nom" name="nom"><span class="error">* <?php echo $nomErr;?></span>
		<br><br>
		<label for="email">E-mail:</label>
		<input type="text" id="email" name="email"><span class="error">* <?php echo $emailErr;?></span>
		<br><br>
		<label for="estudis">Estudis:</label>
		<select id="estudis" name="estudis">    
       		<option value="eso" selected="selected">ESO</option>
       		<option value="batxillerat">BATXILLERAT</option>
       		<option value="smx">SMX</option>
       		<option value="dam">DAM</option>
       		<option value="asix">ASIX</option>
       		<option value="daw">DAW</option>
  		 </select><span class="error">* <?php echo $estudisErr;?></span>
		<br><br>
		<label for="comentari">Comentari:</label>
		<br><textarea id="comentari" name="comentari" rows="5" cols="40"></textarea><span class="error">* <?php echo $comentariErr;?></span>
		<br><br>
		<input type="submit" name="submit" value="Submit"> 
		</fieldset>
</form>	
<?php
	echo "nom:" . $_POST["nom"] . " <br>email:" . $_POST["email"] . " <br>estudis:" . $_POST["estudis"] . " <br>comentari" . $_POST["comentari"];
?>
</body>
</html>
